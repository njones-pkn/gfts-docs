
Products Service
----------------

Background
^^^^^^^^^^
Fare products are used in Parkeon systems to represent the different products and 
the actions that can be carried out on them, such as:

.. hlist::
   :columns: 1

   * Product Sale and Create
   * Use
   * Top-up/Recharge

The products supported by the system have different definitions depending on the 
type of product. The products that are typically found in a Parkeon system are as 
follows:

.. hlist::
   :columns: 1

   * Paper Tickets
   * Flash Passes
   * Magnetic cards [may now be obsolete]
   * Smart Cards that belong to a scheme based on the Wayfarer proprietary format
   * Smart Cards that belong to the ITSO scheme


Product definitions are required within all of the Parkeon devices; TVMs, Rail 
TVMs, ETMs, PVs etc.  As Multi-Modal transport networks become more prevalent, 
so multi-modal products will be required.  All products must have a concept of 
'mode of transport' where a list of modes for which a product is valid is stored
with the product. The product handling should take account of this.  The 
intention is that fare product definitions are used across all of the devices in
the Parkeon family of devices.  This includes small footprint devices like 
ticket machines and validators as well as hand held devices, POS systems and 
TVMs.  It would be beneficial to include features required by Rail products.

Scenarios
^^^^^^^^^

 The examples given in the scenarios below are not the definitive way in which 
 products are used within the system.  The business rule that applies to a 
 particular product is completely dependent on a customer's requirements, so 
 where an example is given, it is not the only possibility. 
 
Smart card use (immediate)
""""""""""""""""""""""""""
     Typically a cardholder presents their smartcard to a reader and it is 
     required to be validated immediately and without operator intervention.  
     This may result in a message being shown on a display,  a gate being 
     opened or a ticket being printed.  The key to this form of use is speed.  
     From the time the card is presented to the time that the gate is opened 
     or ticket printed must be as quick as possible.  Typically from start to 
     end (card identified to the end of printing a ticket) should be sub-500ms 
     to be acceptable.  Customers typically specify the transaction times 
     required as part of Acceptance Testing, but these are typically imprecise 
     and may or may not include printing and card removal.  The essential fact 
     is that for immediate issue transactions, there is an 'intuitive' feel 
     about a transaction and it is very easy to spot when it is too slow.  There
     is also a very real speed requirement when gates have to be opened, since a
     card holder may be walking forwards as they present their card, so the gate 
     must be opened promptly.  This is especially true in 'rush hour' situations.  
     
     **Perception that a card transaction is slow can seriously undermine a 
     customer's confidence in a system and so the importance of speed cannot be 
     underestimated.**

     In this case, there would be a single product look up for each product read 
     from the product on the card.  With this in mind, it is important that 
     product lookups are as quick as possible. therefore handling products as a 
     micro-service should be considered carefully.  In the context of a device 
     that handles immediate product validation it may be better to handle 
     products in-process  in business rules rather than looking them up via a 
     messaging API.

Smart card use (non-immediate)
""""""""""""""""""""""""""""""
     Typically a cardholder presents their card to a reader and there are no 
     valid immediate issue products on the card.  Determination of this would 
     be handled by a card processing business rule.  An example of this might 
     be where a card has an immediate issue pass which at the time of 
     presentation is not valid and also a multi-journey product that is not 
     defined as immediate issue.  As an example, take the following scenario for
     processing a multi-journey product:  After the cardholder presents their 
     card to the reader and no valid immediate issue products are found, the 
     operator of the machine would be presented with a view of the multi-
     ourney product showing the number of journeys remaining and the expiry 
     date of the card.  On pressing a key, a journey is deducted and a ticket 
     is printed.  Once again, product look up speed is an important 
     consideration, since there lookups must be minimised if they are done 
     through the service API.

Smart Product purchase (operator initiated):
""""""""""""""""""""""""""""""""""""""""""""
Smart Product purchase (passenger initiated):
"""""""""""""""""""""""""""""""""""""""""""""

      The user (passenger or driver) of the device is directed in their product 
      selection and purchase by a sequence of UI pages containing lists of 
      items to select from.  Fields shown on the UI pages will be populated in 
      part by the result of queries to the  Fare Product Service.  Typically the
      machines will be a POS based on an Wayfarer 6, or a TVM.  The screens will 
      require several product details to be retrieved so that several categories 
      of products or detailed products can be displayed in a grid or list or on 
      buttons.  The API to the product service will facilitate narrowing down 
      searches until an individual product is identified.

Smart Product top-up (operator initiated):
""""""""""""""""""""""""""""""""""""""""""
Smart Product top-up (passenger initiated):
"""""""""""""""""""""""""""""""""""""""""""
      In this scenario, the products that can be topped up will have been 
      identified from the card and the machine user will be presented with a 
      list of available products.  The user will select a product and a list of 
      available top-ups will be displayed allowing the required top up to be 
      selected.

Smart Product purchase (action list initiated):
"""""""""""""""""""""""""""""""""""""""""""""""
      The product to be purchased will be identified by an item in an action list.  
      Action List items are applied when a card is first presented to the reader 
      and are executed without use intervention.  Typically, a template would be 
      used for creating a product, with only a few specific details sent in the 
      action list being used to overlay into the template as required.  The 
      action item would contain data such as the expiry date of the product, or 
      the number of journeys to initially store on the product, or the initial 
      balance in the case of a purse product.  The product service API for 
      purchasing a product would enable the retrieval of the template data for 
      the specified product.

Smart Product top-up (action list initiated):
"""""""""""""""""""""""""""""""""""""""""""""
      In this scenario, a card is identified as being in the action list and the 
      action is that a product with a  particular Id is topped up.  Typically, 
      this will be a single call to the API, since the Id of the product 
      required is already known.

Non-Smart Product purchases (operator initiated):
"""""""""""""""""""""""""""""""""""""""""""""""""
      As an operator initiated operation not related to a smart card product, 
      this may be a paper ticket for example.  The Operator will select the 
      required product from sequences of menus and grids, possibly narrowing 
      down to a grid showing available fares showing available fares.  The 
      selection criteria will be based on a type of passenger such as Adult, 
      Child, Concession etc and they will be able to select a type of journey 
      such as single, return, day ticket etc.  For rail systems, a journey class
      will be required to identify First, Second etc. classes of travel.

Non-Smart Product purchase (passenger initiated):
"""""""""""""""""""""""""""""""""""""""""""""""""
      A non-smart card product purchase.  This will probably require navigation 
      through several menus, in the same way as a passenger initiated Smart 
      Product purchase or top-up.


Product Structure
^^^^^^^^^^^^^^^^^

The fare product data has the following structure:

​There is a point for discussion, which that of whether the audit, ticket and 
rule data should be separated from the main product data or whether it should 
be flattened so that when queried the result should include all relevant data.  
The trade-off is between the number of queries required to get the data and the
amount of data returned by a single query.  The case for multiple queries based 
on an initial query is that there would be less data retrieved that may not be 
required at a later stage, for example audit and ticket data is only required 
if a product is used as part of a transaction, and rule data would only be 
required if there is  a requirement to use the product that has been selected.  
The downside of this is that there would be up to four queries for a product 
transaction.  The case for a single query that returns all of the required data 
is that this would involve fewer queries, however the downside is that there is 
more potentially redundant data being  transferred.  Currently the API assumes 
the multiple query model, however this could be adapted as required.

A product definitions encapsulates the following:

Identifier:
"""""""""""
      A unique key in the scheme by which the product can be
      identified.
    
Product Name:
"""""""""""""
      A description of the product used by the UI.
    
Product Type:
"""""""""""""
      The type of product (paper, pass, card use, card recharge etc).
    
Ticket properties:
""""""""""""""""""
      The id of the ticket template to print and any text 
      or variables that should be inserted into fields on the ticket
      
Rule properties:
""""""""""""""""
      Items that are parameters relating to the business rule 
      used to process this product.  For smartcards this includes the business 
      rules to run when a card product is used, created or toped up.  This will 
      include a list of modes for which the card is valid.
      
Audit properties:
"""""""""""""""""
      How the product should be audited.  Required for the 
      back office to categorise the product correctly and defines which of the 
      running totals in the device should be updated
      
Search properties:
""""""""""""""""""
      How the product is identified within the system.  For 
      non-smart card product use,  this will be used by the UI to fill in 
      features such as lists and drop down menus of  products.  With smartcard 
      use products, the search criteria is related to certain fields read from 
      the card's products - for instance in a Wayfarer card scheme, this is 
      FieldId, GroupId and Typ, for ITSO products it defines the OID, Typ, PTyp.
      
Service restrictions:
"""""""""""""""""""""
      Details of the product's validity on particular 
      services, direction of travel or within particular time bands.

Product Groups
^^^^^^^^^^^^^^

   Products can be collected together into Groups.  Groups can be used to 
   collect products together by some common feature such as the mode of 
   transport on which they are allowed.  Product families can also be defined in
   this way, such as all 'Day Tickets', which can then be sub categorised as 
   "Adult", "Child", "Concession" etc.

Service API
^^^^^^^^^^^

.. literalinclude:: D:/_PKN/_GFTS_SRC/DEVELOPMENT/Common/MicroServices/FareProductsService/Service/FareProductsService.service_gen



+----------------+-----------------+---------------+
|:ref:`genindex` | :ref:`modindex` | :ref:`search` |
+----------------+-----------------+---------------+
