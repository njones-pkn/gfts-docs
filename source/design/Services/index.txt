.. GFTSDoc documentation master file, created by
   sphinx-quickstart on Mon Nov  5 12:25:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Microservices
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   abt.rst.txt
   PageService.rst.txt
   ProductsService.rst.txt

+----------------+-----------------+---------------+
|:ref:`genindex` | :ref:`modindex` | :ref:`search` |
+----------------+-----------------+---------------+
