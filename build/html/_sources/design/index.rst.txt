.. GFTSDoc documentation master file, created by
   sphinx-quickstart on Mon Nov  5 12:25:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GFTS Design
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Services/index.rst

+----------------+-----------------+---------------+
|:ref:`genindex` | :ref:`modindex` | :ref:`search` |
+----------------+-----------------+---------------+
