.. GFTSDoc documentation master file, created by
   sphinx-quickstart on Mon Nov  5 12:25:38 2018.

GFTS Overview
=============

This section provides some background to GFTS.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./background.rst
   ./microservices.rst
   ./environment.rst


+----------------+-----------------+---------------+
|:ref:`genindex` | :ref:`modindex` | :ref:`search` |
+----------------+-----------------+---------------+

